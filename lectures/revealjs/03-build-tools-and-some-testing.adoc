= Build tools. Introduction to testing 
:customcss: slides.css
:icons: font

++++
	<img id="main-logo" class="main-logo" src="images/template/main_logo_eng_no_text.png" width="300" alt="ntnu logo"/>
++++

[.smaller-80][.center-paragraph]
IT1901 Fall 2019 - 3rd Lecture

[background-color = "#124990"]
[color = "#fff6d5"]
== Overview
[.smaller-80]
- Feedback from last lecture
- Build tools
- Gradle
- Testing
- JUnit
- TestFX
- Mockito
- Jacoco

[background-color = "#124990"]
[color = "#fff6d5"]
== Feedback from last lecture


== !

[.center-paragraph]
image::../images/lecture03/q1.png[width=700]


== !

[.center-paragraph]
image::../images/lecture03/q2.png[width=700]


== !

[.center-paragraph]
image::../images/lecture03/q3.png[width=700]


== Feedback
[.left]
Go to: +
app.one2act.no +
Session: +
KIPAT +

[.right]
image::../images/lecture02/qr-app-one2act-no.svg[width=400]

== Example code on gitlab

Have you managed to build and run the example?

- A) Yes
- B) No

== Deliverable 1

Have you selected the domain for your project?

- A) Yes
- B) No


=== Deliverable 1

programmering av en enkel app,  bruk av gradle til bygging, og git og gitlab til kodehåndtering
Krav til innleveringen:

- Kodingsprosjektet skal ligge i repoet på gitlab 
- Prosjektet skal være konfigurert til å bygge med gradle

=== Deliverable 1

- En README.md-fil på rotnivå i repoet skal beskrive repo-innholdet, spesielt hvilken mappe inni repoet som utgjør kodingsprosjektet.
- En README.md-fil (evt. en fil som README.md lenker til) inni prosjektet skal beskrive hva appen handler om og er ment å gjøre (når den er mer eller mindre ferdig). Ha med et illustrerende skjermbilde, så det er lettere å forstå. Det må også være minst én brukerhistorie for funksjonaliteten dere starter med.

=== Deliverable 1

- Det må ligge inne (i gitlab) utviklingsoppgaver (issues) tilsvarende brukerhistorien, hvor hver utviklingsoppgave må være egnet til å utføres som en egen enhet. De som er påbegynt må være tilordnet det gruppemedlemmet som har ansvaret.

=== Deliverable 1

- Vi stiller ikke krav om at dere er kommet så langt, men det må i hvert fall være noe i hvert av de tre arkitekturlagene, domenelogikk, brukergrensesnitt (JavaFX-GUI) og persistens (fillagring, men ikke nødvendigvis JSON), slik at appen kan kjøres og vise frem "noe". For at det skal være overkommelig, er det viktig at domenet er veldig enkelt i første omgang. Det er viktigere at det som er kodet er ordentlig gjort. Koden som er sjekket inn bør være knyttet til tilsvarende utviklingsoppgave.

=== Deliverable 1
- Gradle skal være konfigurert så en kan kjøre app-en vha. gradle-oppgaven run. 
- Det må finnes minst én test som kan kjøres med gradle. Bygget skal være rigget til å rapportere testdekningsgrad, som derfor skal være over 0%.
- Bruk simpleexample-prosjektet som inspirasjon, men ikke kopier kode direkte.


== What is the biggest impediment preventing you to move forward with the project?

Write keywords or a short sentence. use "none" if you have no impediments.   


[background-color = "#124990"]
[color = "#fff6d5"]
== Build tools

== Build tools (1)

[%step]
- Automate the process of building executable programs from source files
- Packaging binaries required for deployment / distribution
- Run automated tests

== Build tools (2)

[%step]
- Build automation is a necessity to enable CI/CD
- Remove the need to do redundant tasks
- Improve quality of the software
	** the software builds are prepared in a consistent and predictable manner
	** possible to have data to analyze issues and improve

== Make (1)

[%step]
- Designed by Stuart Feldman
- Released in 1976
- Uses makefiles to describe the actions required to produce the build
- Manages dependencies

[.smaller-40]
https://en.wikipedia.org/wiki/Make_(software)

== Make (2)

[%step]
- Has been rewriten a number of times
- Standard modern implementation is GNU Make
- Used in Linux and Mac OS

== Java world build tools

- Ant with Ivy
- Maven
- Gradle

== Apache ANT

[%step]
- modern build system
- released in 2000
- build files use XML
	** tends to get unmanageable even for small projects
- Apache Ivy for managing dependencies (added later)
	** download over network

[.smaller-40]
http://ant.apache.org

== Apache Maven (1)

[%step]
- released in 2004
- improves on ANT
- build files use also XML but the structure is radically different
- dependency management with automatic downloading over the network is available from release

[.smaller-40]
http://maven.apache.org

== Apache Maven (2)

[%step]
- hides complexity in build files through plugins 
- customizing is hard
- dependency management has issues with conflicting versions of same library  


[background-color = "#124990"]
[color = "#fff6d5"]
== Gradle


== Gradle (1)

[%step]
- released in 2012
- build scripts are written in a domain specific language based on Groovy
	**  Groovy ( http://www.groovy-lang.org/ )
- the build script is named `build.gradle`
- build steps are called "tasks"

[.smaller-40]
https://gradle.org


== Gradle (2)

[%step]
- easy to create own tasks
- uses plugins to hide complexity 
- applying plugins allows access to additional tasks 


== Gradle (3)

[.center-paragraph]
image::../images/lecture03/gradle-tree.png[width=700]

[.smaller-40]
https://guides.gradle.org/creating-new-gradle-builds/


== Exercise

Work with the colleague next to you.
Go to gradle.org and create a "hello world" gradle build using the available documentation.
Time 15'

== Exercise feedback

Have you succeeded to create the required gradle build?

- A) Yes
- B) No


[background-color = "#124990"]
[color = "#fff6d5"]
== Gradle demo


[background-color = "#124990"]
[color = "#fff6d5"]
== Testing

== Testing

[%step]
- is an important part of software development
- a way to ensure software quality
- automated testing allows to develop new features with a minimal effort to check if the software still works as expected
- testing frameworks  

== Testing (2)

[%step]
- design 
- implement
- write automated tests
- run tests
- we do not test just for know, we write tests to keep running them during project life cycle 

== Testing (3)

[%step]
- design tests
- implement the test
- provide inputs
- run the tests
- provide expected outputs
- check if the result  we get matches what we expect
- produce a manageable output that the developer can consult 

== Testing (3)

- design tests
- implement the test
- provide inputs
- *run the tests*
- provide expected outputs
- *check if the result  we get matches what we expect*
- *produce a manageable output that the developer can consult* 


[background-color = "#124990"]
[color = "#fff6d5"]
== JUnit


== JUnit

- Is a Java unit testing framework.
- provides the means to automate test
- allows to eliminate redundant testing tasks  


== JUnit (2)

``` java
import org.junit.*;

public class FoobarTest {
    @BeforeClass
    public static void setUpClass() throws Exception {
        // Code executed before the first test method
    }

    @Before
    public void setUp() throws Exception {
        // Code executed before each test
    }
 
    @Test
    public void testOneThing() {
        // Code that tests one thing
    }

    @Test
    public void testAnotherThing() {
        // Code that tests another thing
    }

    @Test
    public void testSomethingElse() {
        // Code that tests something else
    }

    @After
    public void tearDown() throws Exception {
        // Code executed after each test 
    }
 
    @AfterClass
    public static void tearDownClass() throws Exception {
        // Code executed after the last test method 
    }
}
```

[.smaller-40]
https://en.wikipedia.org/wiki/JUnit


[background-color = "#124990"]
[color = "#fff6d5"]
== TestFX

== TestFX

- testing for JavaFx applications
- provides robots for UI testing
- support for JUnit

[.smaller-40]
https://github.com/TestFX/TestFX


[background-color = "#124990"]
[color = "#fff6d5"]
== Mockito


== Mockito

- mocking is a technique to test functionality in isolation
- mock objects simulate real objects
- return dummy values corresponding to the input given at creation
- Mockito uses reflection features in Java to create the mock objects

[.smaller-40]
https://site.mockito.org/

[background-color = "#124990"]
[color = "#fff6d5"]
== Jacoco

== Jacoco

- tool for assessing code coverage
- does not need modifying code
- can produce a report in html format
- integrates with a number a tools including Gradle

[.smaller-40]
https://www.jacoco.org/



++++
 <div id="footer" class="footer">
 	<div style="display:table-row;">
     <span class="element" style="width:150px;">
     	<a href="https://www.ntnu.no" target="_blank">
     		<img 	id="footer-logo" class="footer-logo" 
     				src="images/template/logo_ntnu.png" 
     				alt="ntnu logo" height="28"/>
     	</a>
     </span>
     <span class="element" style="width:300px;">| IT1901 - 3rd lecture </span>
     <span class="element">| Build tools. Introduction to testing </span>
     <span class="element">&nbsp;&nbsp;&nbsp;&nbsp;</span>
  </div>   
 </div>
 
 <div id="vertical-ntnu-name" class="vertical-ntnu-name">
 	<span class="helper"></span>
 	<img src="images/template/vertical-ntnu-name.png" alt="Norwegian University of Science and Technology" />
 </div>
 
 <script type="text/javascript">
     window.addEventListener("load", function() {
         revealDiv = document.querySelector("body div.reveal")
         footer = document.getElementById("footer");
         revealDiv.appendChild(footer);
         
         titleSlideDiv = document.querySelector("div.slides section.title")
         mainLogo = document.getElementById("main-logo");
         titleSlideDiv.prepend(mainLogo);
         
         vertName =  document.getElementById("vertical-ntnu-name");
         revealDiv.appendChild(vertName);
     } );
 </script>
++++