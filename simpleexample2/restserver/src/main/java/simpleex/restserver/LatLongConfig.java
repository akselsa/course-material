package simpleex.restserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import simpleex.core.LatLongs;
import simpleex.restapi.LatLongObjectMapperProvider;
import simpleex.restapi.LatLongsService;

public class LatLongConfig extends ResourceConfig {

  public LatLongConfig() {
    this(new LatLongs());
  }

  private static LatLongs readValue(final String json) {
    try {
      final LatLongs latLongs = new LatLongObjectMapperProvider().getContext(ObjectMapper.class).readValue(json, LatLongs.class);
      System.out.println("Read " + json + " as " + latLongs);
      return latLongs;
    } catch (final Exception e) {
      System.out.println("Exception when reading " + json + ": " + e);
    }
    return null;
  }

  public LatLongConfig(final String json) throws IOException {
    this(readValue(json));
  }

  public LatLongConfig(final LatLongs latLongs) {
    System.out.println("Serving " + latLongs.toList());
    register(LatLongsService.class);
    register(LatLongObjectMapperProvider.class);
    register(JacksonFeature.class);

    register(new AbstractBinder() {
      @Override
      protected void configure() {
        bind(latLongs);
      }
    });
  }
}
