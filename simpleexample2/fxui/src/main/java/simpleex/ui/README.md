# Kildekode for brukergrensesnittet

Brukergrensesnittet er laget med JavaFX og FXML.

Brukergrensesnittet er enkelt og består av en liste og et kart. Lista viser innholdet i et **LatLongs**-objekt, altså en samling **LatLong**-objekter. Når man velger et element, så vises det elementet som et punkt på kartet. En kan flytte punktet, forskyve kartet, zoome inn og ut og opprette nye **LatLong**-objekter. 

Kartet implementeres av ([FxMapControl](https://github.com/ClemensFischer/FX-Map-Control), og siden biblioteket ikke er tilgjengelig i et sentral repository, så har vi bygget og lagt ved jar-filen.

## Arkitektur

Arkitekturen er som de fleste JavaFX-apper som bruker FXML, vi har en oppstartsklasse (**FxApp**) som arver fra **Application** og som stort sett bare laster inn og viser frem (innholdet i) en FXML-fil. FXML-fila angir en kontroller-klasse (med **fx:controller**-attributtet i rot-elementet) hvor UI-logikken ligger. 

Det finnes to varianter av applikasjonen, én som bruker lokale data lagret på fil og én som bruker sentrale data håndtert gjennom et REST-API på en server. Derfor har vi også to FXML-filer (**FxApp.fxml** og **FxAppUsingRest.fxml**) og to tilhørende kontroller-klasser (**FxAppController** og **FxAppUsingRestController**). For å gjøre det enklere å ha en del felles kode i en abstrakt kontroller-klasse (**AbstractFxAppController**), så er datatilgangen skilt ut i egne klasser (**LocalLatLongsDataAccess** og **RestLatLongsDataAccess**) som implementerer et felles **LatLongsDataAccess**-grensesnitt.

Det er bare én app-klasse (**FxApp**), og denne velger hvilken variant som brukes, basert på om det oppgis en server-URL som kommandolinjeargument. Hvis en f.eks. kjører 'java FxApp http://localhost:8080/' så kjøres varianten som bruker REST-API-et. Det betyr også at serveren må startes. I tillegg til URL-en så kan en også oppgi initielle LatLongs-data kodet som json. Dette gjør det enklere å sjekke at data leses riktig fra REST-API-et ved oppstart. **build.gradle** er rigget slik at begge disse kommandolinjeargumentene oppgis ved kjøring av **Run**-oppgaven. 

```plantuml
class AbstractFxAppController
interface LatLongsDataAccess
class LocalLatLongsDataAccess
class RestLatLongsDataAccess
class BorderPane
class "ListView<LatLong>" as ListView
class "fxmapcontrol.MapBase" as MapBase

BorderPane *--> ListView: "left"
BorderPane *--> MapBase: "center"

AbstractFxAppController --> LatLongsDataAccess: "dataAccess"
AbstractFxAppController --> MapBase: "mapView"
AbstractFxAppController --> ListView: "locationListView"

FxAppController --|> AbstractFxAppController
FxAppUsingRestController --|> AbstractFxAppController

LocalLatLongsDataAccess ..|> LatLongsDataAccess
RestLatLongsDataAccess ..|> LatLongsDataAccess
```
