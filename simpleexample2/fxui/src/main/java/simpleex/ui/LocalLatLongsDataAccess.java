package simpleex.ui;

import java.util.Collection;
import simpleex.core.LatLong;
import simpleex.core.LatLongs;

/*
@startuml
class FxAppController
class LatLongs
class BorderPane
class "ListView<LatLong>" as ListView
class "fxmapcontrol.MapBase" as MapBase

BorderPane *--> ListView: "left"
BorderPane *--> MapBase: "center"

FxAppController --> LatLongs: "latLongs"
FxAppController --> MapBase: "mapView"
FxAppController --> ListView: "locationListView"
@enduml
 */

/**
 * The controller for the app.
 * @author hal
 *
 */
public class LocalLatLongsDataAccess implements LatLongsDataAccess {

  private LatLongs latLongs;

  /**
   * Initializes the data access.
   */
  public LocalLatLongsDataAccess() {
    this(new LatLongs());
  }

  /**
   * Initializes the data access with a specific LatLongs.
   * @param latLongs the LatLongs object to use
   */
  public LocalLatLongsDataAccess(final LatLongs latLongs) {
    this.latLongs = latLongs;
  }

  /**
   * Gets the LatLongs objects used by the controller.
   * @return the controller's LatLongs objects
   */
  public LatLongs getLatLongs() {
    return latLongs;
  }

  /**
   * Sets the LatLongs objects used by the controller.
   * @param latLongs the LatLongs object to use
   */
  public void setLatLongs(final LatLongs latLongs) {
    this.latLongs = latLongs;
  }

  @Override
  public Collection<LatLong> getAllLatLongs() {
    return getLatLongs().toList();
  }

  @Override
  public LatLong getLatLong(final int num) {
    return getLatLongs().getLatLong(num);
  }

  @Override
  public void setLatLong(final int index, final LatLong latLong) {
    getLatLongs().setLatLong(index, latLong);
  }

  @Override
  public int addLatLong(final LatLong latLong) {
    return getLatLongs().addLatLong(latLong);
  }
}
