package simpleex.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.FileChooser;
import simpleex.core.LatLongs;

/*
@startuml
class FxAppController
class LatLongs
class BorderPane
class "ListView<LatLong>" as ListView
class "fxmapcontrol.MapBase" as MapBase

BorderPane *--> ListView: "left"
BorderPane *--> MapBase: "center"

FxAppController --> LatLongs: "latLongs"
FxAppController --> MapBase: "mapView"
FxAppController --> ListView: "locationListView"
@enduml
 */

/**
 * The controller for the app.
 * @author hal
 *
 */
public class FxAppController extends AbstractFxAppController {

  /**
   * Initializes the controller.
   */
  public FxAppController() {
    setDataAccess(new LocalLatLongsDataAccess(new LatLongs()));
  }

  /**
   * Sets the LatLongs objects used by the controller.
   * @param latLongs the LatLongs object to use
   */
  public void setLatLongs(final LatLongs latLongs) {
    setDataAccess(new LocalLatLongsDataAccess(latLongs));
    updateLocationViewList(0);
  }

  // File menu items

  private FileChooser fileChooser;

  private FileChooser getFileChooser() {
    if (fileChooser == null) {
      fileChooser = new FileChooser();
    }
    return fileChooser;
  }

  @FXML
  void handleOpenAction(final ActionEvent event) {
    final FileChooser fileChooser = getFileChooser();
    final File selection = fileChooser.showOpenDialog(null);
    if (selection != null) {
      try (InputStream input = new FileInputStream(selection)) {
        setLatLongs(getObjectMapper().readValue(input, LatLongs.class));
      } catch (final IOException e) {
        showExceptionDialog("Oops, problem when opening " + selection, e);
      }
    }
  }

  private void showSaveExceptionDialog(final File location, final Exception e) {
    showExceptionDialog("Oops, problem saving to " + location, e);
  }

  @FXML
  void handleSaveAction() {
    final FileChooser fileChooser = getFileChooser();
    final File selection = fileChooser.showSaveDialog(null);
    if (selection != null) {
      try (OutputStream outputStream = new FileOutputStream(selection, false)) {
        getObjectMapper().writeValue(outputStream, getDataAccess().getAllLatLongs());
      } catch (final IOException e) {
        showSaveExceptionDialog(selection, e);
      }
    }
  }
}
