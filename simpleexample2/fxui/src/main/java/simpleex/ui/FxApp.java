package simpleex.ui;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.glassfish.grizzly.http.server.HttpServer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import simpleex.core.LatLongs;
import simpleex.restapi.LatLongsService;
import simpleex.restserver.LatLongGrizzlyApp;

public class FxApp extends Application {

  private HttpServer restServer = null;
  @Override
  public void start(final Stage stage) throws Exception {
    URI baseUri = null;
    final List<String> args = getParameters().getRaw();
    if (args.size() >= 1) {
      final List<String> serverArgs = new ArrayList<String>();
      baseUri = URI.create(args.get(0));
      serverArgs.add(baseUri.toString());
      if (args.size() >= 2) {
        // json of initial data
        serverArgs.add(args.get(1));
      }
      restServer = LatLongGrizzlyApp.startServer(serverArgs.toArray(new String[serverArgs.size()]), 5);
    }
    final String fxml = (baseUri != null ? "FxAppUsingRest.fxml" : "FxApp.fxml");
    final FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxml));
    final Parent root = fxmlLoader.load();
    if (baseUri == null) {
      // set initial data manually
      final FxAppController controller = fxmlLoader.getController();
      controller.setLatLongs(new LatLongs(63.1, 11.2, 63.2, 11.0));
    } else {
      final FxAppUsingRestController controller = fxmlLoader.getController();
      controller.setDataAccess(new RestLatLongsDataAccess(baseUri + LatLongsService.LAT_LONG_SERVICE_PATH, controller.getObjectMapper()));
    }
    final Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.show();
  }

  @Override
  public void stop() throws Exception {
    if (restServer != null) {
      restServer.shutdown();
    }
    super.stop();
  }

  /**
   * Launches the app.
   * @param args the command line arguments
   */
  public static void main(final String[] args) {
    // only needed on ios
    System.setProperty("os.target", "ios");
    System.setProperty("os.name", "iOS");
    System.setProperty("glass.platform", "ios");
    System.setProperty("targetos.name", "iOS");
    launch(args);
  }
}
