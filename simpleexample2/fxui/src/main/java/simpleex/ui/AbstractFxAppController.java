package simpleex.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import fxmapcontrol.Location;
import fxmapcontrol.MapBase;
import fxmapcontrol.MapItemsControl;
import fxmapcontrol.MapNode;
import fxmapcontrol.MapProjection;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import simpleex.core.LatLong;
import simpleex.json.LatLongsModule;

/*
@startuml
class AbstractFxAppController
class LatLongs
class BorderPane
class "ListView<LatLong>" as ListView
class "fxmapcontrol.MapBase" as MapBase

BorderPane *--> ListView: "left"
BorderPane *--> MapBase: "center"

AbstractFxAppController --> MapBase: "mapView"
AbstractFxAppController --> ListView: "locationListView"
@enduml
 */

/**
 * The controller for the app.
 * @author hal
 *
 */
public abstract class AbstractFxAppController {

  private LatLongsDataAccess dataAccess;

  protected LatLongsDataAccess getDataAccess() {
    return dataAccess;
  }

  protected void setDataAccess(final LatLongsDataAccess dataAccess) {
    this.dataAccess = dataAccess;
    if (locationListView != null) {
      updateLocationViewList(0);
    }
  }

  @FXML
  private ListView<LatLong> locationListView;

  @FXML
  private MapBase mapView;

  private MapItemsControl<MapNode> markersParent;
  private MapMarker marker = null;
  private DraggableNodeController draggableMapController = null;
  private DraggableNodeController draggableMarkerController = null;

  @FXML
  private Slider zoomSlider;

  @FXML
  private void initialize() {
    // map stuff
    // mapView.getChildren().add(MapTileLayer.getOpenStreetMapLayer());
    zoomSlider.valueProperty()
      .addListener((prop, oldValue, newValue) -> mapView.setZoomLevel(zoomSlider.getValue()));
    zoomSlider.setValue(8);
    markersParent = new MapItemsControl<MapNode>();
    mapView.getChildren().add(markersParent);
    draggableMapController = new DraggableNodeController(this::handleMapDragged);
    draggableMapController.setImmediate(true);
    draggableMapController.attach(mapView);
    draggableMarkerController = new DraggableNodeController(this::handleMarkerDragged);
    // the location list
    locationListView.getSelectionModel().selectedIndexProperty()
      .addListener((prop, oldValue, newValue) -> updateMapMarker(true));
  }

  private void handleMapDragged(final Node node, final double dx, final double dy) {
    final MapProjection projection = mapView.getProjection();
    final Point2D point = projection.locationToViewportPoint(mapView.getCenter());
    final Location newCenter = projection.viewportPointToLocation(point.add(-dx, -dy));
    mapView.setCenter(newCenter);
  }

  private void handleMarkerDragged(final Node node, final double dx, final double dy) {
    final MapProjection projection = mapView.getProjection();
    final Point2D point = projection.locationToViewportPoint(marker.getLocation());
    final Location newLocation = projection.viewportPointToLocation(point.add(dx, dy));
    dataAccess.setLatLong(locationListView.getSelectionModel().getSelectedIndex(),
        location2LatLong(newLocation));
    updateLocationViewListSelection(false);
  }

  private LatLong location2LatLong(final Location newLocation) {
    return new LatLong(newLocation.getLatitude(), newLocation.getLongitude());
  }

  private void updateMapMarker(final boolean centerOnMarker) {
    final int num = locationListView.getSelectionModel().getSelectedIndex();
    if (num < 0) {
      markersParent.getItems().clear();
      if (draggableMarkerController != null) {
        draggableMarkerController.detach(marker);
      }
      marker = null;
    } else {
      final LatLong latLong = dataAccess.getLatLong(num);
      if (marker == null) {
        marker = new MapMarker(latLong);
        markersParent.getItems().add(marker);
        if (draggableMarkerController != null) {
          draggableMarkerController.attach(marker);
        }
      } else {
        marker.setLocation(latLong);
      }
      if (centerOnMarker) {
        mapView.setCenter(marker.getLocation());
      }
    }
  }

  @FXML
  void handleAddLocation() {
    final Location center = mapView.getCenter();
    final LatLong latLong = location2LatLong(center);
    final int pos = dataAccess.addLatLong(latLong);
    updateLocationViewList(pos);
  }

  private void updateLocationViewListSelection(final Boolean updateMapMarker) {
    final int selectedIndex = locationListView.getSelectionModel().getSelectedIndex();
    locationListView.getItems().set(selectedIndex, dataAccess.getLatLong(selectedIndex));
    if (updateMapMarker != null) {
      updateMapMarker(updateMapMarker);
    }
  }

  protected void updateLocationViewList(int selectedIndex) {
    final LatLong[] latLongs = dataAccess.getAllLatLongs().toArray(new LatLong[0]);
    final int oldSelectionIndex = locationListView.getSelectionModel().getSelectedIndex();
    locationListView.setItems(FXCollections.observableArrayList(latLongs));
    if (selectedIndex < 0 || selectedIndex >= latLongs.length) {
      selectedIndex = oldSelectionIndex;
    }
    if (selectedIndex >= 0 && selectedIndex < latLongs.length) {
      locationListView.getSelectionModel().select(selectedIndex);
    }
  }

  private ObjectMapper objectMapper;

  /**
   * Gets the ObjectMapper used by this controller.
   * @return the ObjectMapper used by this controller
   */
  public ObjectMapper getObjectMapper() {
    if (objectMapper == null) {
      objectMapper = new ObjectMapper();
      objectMapper.registerModule(new LatLongsModule());
    }
    return objectMapper;
  }

  protected void showExceptionDialog(final String message) {
    final Alert alert = new Alert(AlertType.ERROR, message, ButtonType.CLOSE);
    alert.showAndWait();
  }

  protected void showExceptionDialog(final String message, final Exception e) {
    showExceptionDialog(message + ": " + e.getLocalizedMessage());
  }
}
