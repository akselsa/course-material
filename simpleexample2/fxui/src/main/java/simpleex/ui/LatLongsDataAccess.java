package simpleex.ui;

import java.util.Collection;
import simpleex.core.LatLong;

/**
 * Data access methods used by the controller.
 * @author hal
 *
 */
public interface LatLongsDataAccess {

  /**
   * Gets all the (internal) LatLong objects.
   * @return the (internal) LatLong objects
   */
  Collection<LatLong> getAllLatLongs();

  /**
   * Gets a specific LatLong object by index.
   * @param num the index of the LatLong object to get
   * @return the LatLong object at the specified index
   */
  LatLong getLatLong(int num);

  /**
   * Sets a the LatLong object at a specific index.
   * @param num the index of the LatLong object to set
   * @param latLong the new LatLong object
   */
  void setLatLong(int index, LatLong latLong);

  /**
   * Adds a LatLong object
   * @param latLong the LatLong object to add
   * @return the index where the LatLong object was added
   */
  int addLatLong(LatLong latLong);
}
