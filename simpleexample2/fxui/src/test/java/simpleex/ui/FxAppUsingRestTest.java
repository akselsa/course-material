package simpleex.ui;

import java.io.IOException;
import java.net.URL;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import simpleex.restapi.LatLongsService;
import simpleex.restserver.LatLongGrizzlyApp;

public class FxAppUsingRestTest extends AbstractFxAppTest {

  @Override
  protected URL getFxmlResource() {
    return getClass().getResource("FxAppUsingRest.fxml");
  }

  private LatLongsDataAccess dataAccess;

  @Override
  protected LatLongsDataAccess getDataAccess() {
    return dataAccess;
  }

  private HttpServer currentServer;

  @Override
  protected void setUpLatLongsDataAccess() {
    final String serverUrlString = "http://localhost:8080/";
    final String clientUrlString = serverUrlString + LatLongsService.LAT_LONG_SERVICE_PATH;
    dataAccess = new RestLatLongsDataAccess(clientUrlString, controller.getObjectMapper());
    try {
      currentServer = LatLongGrizzlyApp.startServer(new String[] {
          serverUrlString,
          "[[63.1, 11.2], [63.2, 11.0]]"
      }, 5);
    } catch (final IOException e) {
      throw new IllegalStateException("Couldn't setup server");
    }
  }

  @After
  public void stopServer() {
    currentServer.shutdownNow();
  }
}
