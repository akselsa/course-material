# Testkode for brukergrensesnittet

Denne pakken inneholder testkode for JavaFX-app-en. Koden er ikke så grundig...

Det mest spesielle er at det finnes to varianter (subklasser av **AbstractFxAppTest**), én for hver variant av app-en, altså med eller uten bruk av REST-API-et. Varianter som bruker REST-API-et (**FxAppUsingRestTest**) starter opp serveren ifm. rigging av datatilgangsobjektet (instans av **LatLongsDataAccess**) og stopper den i nedriggingsfasen.
