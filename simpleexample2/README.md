# Simpleexample2

Dette prosjektet er et eksempel på en nokså minimalistisk trelagsapplikasjon, altså med et domenelag, brukergrensesnitt (UI) og persistens (lagring). Prosjektet inneholder tester for alle lagene, med rimelig god dekningsgrad.

Prosjektet er konfigurert som et multi-modul-prosjekt med gradle, hvor domene- og persistenslagene ligger i en [kjernemodul (core)](core/README.md) og brukergrensesnittet i en [ui-modul (fxui)](fxui/README.md).

## Bygging med gradle

I et multi-modul-prosjekt så inneholder **settings.gradle** en oversikt over del-prosjektene (sub-modulene) og **build.gradle** konfigurasjonen som er felles for disse. Det som vi har lagt inn av felles konfigurasjon (så langt) er repo-ene hvor avhengighetene finnes.

## Arkitekturdiagram

![Arkitektur](architecture.png)
