# Setting up Java

![](logos/java.png)

To be able to develop in Java one requires a JDK - Java Development Kit. You have to download and properly set up java
For IT1901 either JDK 11 or 12 will do just fine.

Installing manually the jdk has the following steps no matter what operating system you are using:

* download the archive containing the desired release
* extract it
* move it to a meaningful location (depends on the os what is a good location)
* set up environment variables for `JAVA_HOME` and `PATH`
* apply changes if necessary

Note: The examples shown here use filenames as they were for jdk 12.0.2 at the moment of redacting the document. Adjust the filenames and the paths as needed. Depending on your OS version or flavour you might have different text editors and you might need to adjust for that as well.  

## Install java

If there is no Java Development Kit installed or the version is not what you need (we recommend version 11 or 12) then proceed to download JDK and install it.

### Windows

#### Check if there is any java installed

In a command line window type: 

``` batch
java -version
```
![](images/04%20-%20check%20for%20java%20windows.png)

#### Extract downloaded binary

After the download finishes you have in the destination folder the `.zip` file with a name like this `openjdk-12.0.2_windows-x64_bin.zip`. Right click it and extract the contents.

![](images/05%20-%20extract%20openjdk%20windows.png)

Copy the extracted folder in a meaningful location on your hard drive like for example in `C:\jdks` 

![](images/06%20-%20move%20the%20jdk%20folder%20to%20c-jdks%20folder.png)


#### Set up the environment variables    

To properly set up Java on your machine you need to set couple environment variables. To do that you need to open "Environment variables" dialog box.
There are several ways to get to that dialog box.

Right click "This PC" and select "Properties" 

![](images/07%20-%20open%20pc%20properties.png)

You should see a window like the one below. Choose "Advanced System Settings" option in the top left side.

![](images/08%20-%20pc%20properties%20window.png)

A new window will open showing "System properties"

![](images/09%20-%20system%20properties.png)

Open "Environment Variables"

If the JDK should be available for all users than make the environment variable changes at system level otherwise do them at user level.

Check if there is already a `JAVA_HOME` environment variable. If it is change the value to the path to your JDK (`C:\jdks\jdk-12.0.2`)

![](images/10%20-%20env%20java%20home.png)

Next we need to add the `bin` folder within the JDK folder to the `PATH` environment variable so that the java commands are accessible from the command prompt

![](images/11%20-%20env%20path.png)

Finally we can check if java is now running as expected

![](images/12%20-%20java%20check%20ok.png)

##### Notes

* You can check the values of environment variables from command prompt issuing commands such as `SET JAVA_HOME` or `SET PATH`
* You can get to the environment variable dialog by other means
	- search "env" in the windows start menu
	- open System from the Control Panel and then "Environment Variables" 


### Linux

#### Check if there is any java installed

Open a terminal window and type

``` bash
java -version
```

![](images/linux03_check_false.png)


#### Extract downloaded binary

After the download finishes you have in the destination folder the `.tar.gz` file with a name like this `openjdk-12.0.2_linux-x64_bin.tar.gz` . 

Extract the files and place them in the desired location with the following commands


``` bash
tar zxvf openjdk-12.0.2_linux-x64_bin.tar.gz
sudo mkdir /usr/lib/jvm
sudo mv jdk-12.0.2 /usr/lib/jvm/
```

![](images/linux02_extract.png)

![](images/linux04_move.png)


#### Set up the environment variables 

create a shell script in /etc/profile.d

``` bash
sudo nano /etc/profile.d/jdk.sh
```

![](images/linux05_env01.png)


add the following lines inside the file and save and close

``` bash
export JAVA_HOME=/usr/lib/jvm/jdk-12.0.2
export PATH=$PATH:$JAVA_HOME/bin

```

![](images/linux06_env02.png)

apply changes

logout and log back in or run the script you have just created

``` bash
bash /etc/profile.d/jdk.sh
```


check if everything is in order

``` bash
echo $JAVA_HOME
java -version
```

![](images/linux07_check_true.png)

### MacOs

#### Check if there is any java installed

Open a terminal window and type

``` bash
java -version
```

![](images/mac_02_check_false.png)


#### Extract downloaded binary

After the download finishes you have in the destination folder the `.tar.gz` file with a name like this `openjdk-12.0.2_osx-x64_bin.tar.gz`.

Extract the files using the following command

``` bash
tar xf openjdk-12.0.2_osx-x64_bin.tar.gz
```

![](images/mac_03_extract.png)


Move the extracted to the folder  `/Library/Java/JavaVirtualMachines/`
You will need to execute the move with super user privileges in order to put the files in the system folder
 
``` bash
sudo mv jdk-12.0.2.jdk /Library/Java/JavaVirtualMachines/
```

![](images/mac_04_move.png)


#### Set up the environment variables 

To set up the environment variable `JAVA_HOME` we will need to edit the `.bash_profile` file in the home directory.

If the file mentioned does not exist we can create it with the following command:

``` bash
touch ~/.bash_profile
```

![](images/mac_05_env01.png)


We need to export the `JAVA_HOME` variable so we edit the file using

``` bash
edit ~/.bash_profile
```
or 

``` bash
nano ~/.bash_profile
```

![](images/mac_06_env02.png)


we add the following line

`export JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk-12.0.2.jdk/Contents/Home"`

![](images/mac_08_env03.png)


finally we save the changes and close the file. To apply the changes we need to run the following command

``` bash
source ~/.bash_profile
```

![](images/mac_07_apply_env.png)


check if everything is in order

![](images/mac_09_check_true.png)

## Using sdkman

sdkman is a tool that allows you to manage development kits including JDK. It is possible to run it on Linux and Mac without additional software. On windows one can use it after installing Git Bash and MinGW.
Refer to the https://sdkman.io webpage for more info and instructions 

### Installation on supported systems - open a terminal window and enter:

``` bash
curl -s "https://get.sdkman.io" | bash
```

Follow the instructions on-screen to complete installation.

Apply changes by entering:

``` bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
```

check the version using the command

``` bash
sdk version
```

If the tool is now properly installed the result of the previous command will be:

``` bash
 sdkman 5.x.x
```


### Typical usage

After installation you will typically use commands like 

``` bash
sdk list java
```
list the available versions

``` bash
sdk install java
```
install latest java, add version number to install specific java

``` bash
sdk use java 12.0.2
```
use the java development kit with version 12.0.2


See more commands on the sdkman.io site 



## Download Java


###  Open-source reference implementation (GNU GPL License, provided by Oracle)

* 1. go to http://jdk.java.net

![](images/01%20-%20jdk.java.net%20-%20home.png)

* 2. select the general available build (currently 12)

![](images/02%20-%20jdk.java.net%20-%20latest%20ga.png)

* 3. download the binary corresponding to your operating system


### Get binaries from Oracle site (Oracle license)

1. go to https://www.oracle.com/technetwork/java/javase/downloads/index.html
2. sign in (if you do not have an account you will need to create one in order to be allowed to download)
3. select the latest (currently 12)
4. download the binary corresponding to your operating system

There are more choices for downloads. Typically a .zip or .tar.gz would imply manual setup while the .exe, .deb, .rpm, .dmg will provide some sort of installer support.  

For the .zip or .tar.gz files you need to follow the procedure from above.
.exe (windows) - run and follow the installer prompts
.dmg (mac) - run and follow the installer prompts
.deb (linux) - package format used in Debian and Ubuntu linux distributions

```bash
sudo dpkg -i ~/Downloads/jdk-12.0.2_linux-x64_bin.deb
```

alternative way to install .deb packages is with apt

```bash
sudo apt install ~/Downloads/jdk-12.0.2_linux-x64_bin.deb
```
In Ubuntu you can also double click the package and it will open in Software Center where there is an option to install it.

.rpm (linux) - package format commonly used in Redhat and Mandrake linux distributions 

``` bash
rpm -ivh ~/Downloads/jdk-12.0.2_linux-x64_bin.rpm
```
