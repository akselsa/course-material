## Installation
### Install Java

#### [Guide for installing Java Development Kit](https://gitlab.stud.idi.ntnu.no/it1901/course-material/blob/master/howto/get-java.md "Installing Java")


#### Download links

* http://jdk.java.net 
* https://www.oracle.com/technetwork/java/javase/downloads/index.html


#### How and why to uninstall older versions of Java
https://www.java.com/en/download/faq/remove_olderversions.xml

### Install Git
#### Download link
https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

#### For Windows we recommend Git for Windows
https://gitforwindows.org/

### Install Eclipse or Intellij
#### Intellij download link (download the free community edition)
https://www.jetbrains.com/idea/download/#section=mac

#### Eclipse for Java download link 
https://www.eclipse.org/downloads/packages/release/2019-06/r/eclipse-ide-java-developers

## Update the local project credentials to match your Gitlab user.
1) Open the terminal window and go to your Gitlab project
2) Update local user email with 'git config --local user.email YOUR_NTNU_USERNAME@stud.ntnu.no'
3) Update local user password with 'git config --local user.password YOUR_NTNU_PASSWORD'
4) Update local user username with 'git config --local user.name YOUR_NTNU_USERNAME'

## How to clone video 
VIDEO: https://vimeo.com/355521976
