# Simpleexample

Dette prosjektet er et eksempel på en nokså minimalistisk trelagsapplikasjon, altså med et domenelag, brukergrensesnitt (UI) og persistens (lagring). Prosjektet inneholder tester for alle lagene, med rimelig god dekningsgrad. Prosjektet er konfigurert med gradle som byggesystem.

## Organisering av koden

Prosjektet er organisert med 2 * 2 = 4 kildekodemapper, kode og ressurser for henholdsvis applikasjonen selv og testene:

- **src/main/java** for koden til applikasjonen
- **src/main/resources** for tilhørende ressurser, f.eks. data-filer og FXML-filer, som brukes av applikasjonen.
- **src/test/java** for testkoden
- **src/test/resources** for tilhørende ressurser, f.eks. data-filer og FXML-filer, som brukes av testene.

Dette er en vanlig organisering for prosjekter som bygges med gradle (og maven).

## Domenelaget

Domenelaget inneholder alle klasser og logikk knyttet til dataene som applikasjonen handler om og håndterer. Dette laget skal være helt uavhengig av brukergrensesnittet eller lagingsteknikken som brukes.

Vår app handler om samlinger av såkalte geo-lokasjoner, altså steder identifisert med lengde- og breddegrader. Domenelaget inneholder klasser for å representere og håndtere slike, og disse finnes i **[simpleex.core](src/main/java/simpleex/core/README.md)**-pakken.

## Brukergrensesnittlaget

Brukergrensesnittlaget inneholder alle klasser og logikk knyttet til visning og handlinger på dataene i domenelaget. Brukergrensesnittet til vår app viser frem en liste av geo-lokasjoner, den som velges vises på et kart. En flytte og legge til geo-lokasjoner. Samlingen med geo-lokasjoner kan lagres og evt. leses inn igjen.

Brukergrensesnittet er laget med JavaFX og FXML og finnes i **[simpleex.ui](src/main/java/simpleex/ui/README.md)**-pakken (JavaFX-koden i **src/main/java** og FXML-filen i **src/main/resources**)

## Persistenslaget

Persistenslaget inneholder alle klasser og logikk for lagring (skriving og lesing) av dataene i domenelaget. Vårt persistenslag implementerer fillagring med JSON-syntaks.

Persistenslaget finnes i **[simpleex.json](src/main/java/simpleex/json/README.md)**-pakken.

## Bygging med gradle

For litt mer komplekse prosjekter, er det lurt å bruke et såkalt byggeverktøy, f.eks. gradle eller maven, for å automatisere oppgaver som kjøring av tester, sjekk av ulike typer kvalitet osv. Vårt prosjekt er konfigurert til å bruke [gradle](https://gradle.org), og følgelig har prosjektet en del filer og mapper spesifikt for det:

- gradle, gradlew og gradlew.bat - mappe og filer for kjøring av gradle, som regel rigget opp en gang for alle ved opprettelse av prosjektet 
- settings.gradle - inneholder navnet på prosjektet og evt. navnet på mapper for delprosjekter (ikke brukt her)
- build.gradle - konfigurasjon av gradle-tillegg basert på typen prosjekt

Av disse er det build.gradle som krever mest forklaring, settings.gradle trenger bare å inneholde én linje, som navngir prosjektet: `rootProject.name = 'simpleexample'`. For litt bakgrunn om bygging generelt og gradle spesielt, se [her](ci-gradle.md).

Vårt gradle-bygg er satt opp med tillegg for java-applikasjoner generelt (**application**) og med JavaFX spesielt (**org.openjfx.javafxplugin**). Vi trenger (minst) java-versjon 10 og javafx-version 11.

I tillegg bruker vi diverse kodekvalitetsanalyseverktøy ([jacoco](https://github.com/jacoco/jacoco) med **[jacoco](https://docs.gradle.org/current/userguide/jacoco_plugin.html)**, [PMD](https://pmd.github.io) med **[pmd](https://docs.gradle.org/current/userguide/pmd_plugin.html)**, [spotbugs](https://spotbugs.github.io) med **[com.github.spotbugs](https://spotbugs.readthedocs.io/en/latest/gradle.html)** og [checkstyle](https://checkstyle.sourceforge.io) med **[checkstyle](https://docs.gradle.org/current/userguide/checkstyle_plugin.html)**). Disse er satt opp slik at de ikke stopper bygget om ikke alt er på stell. spotbugs er satt opp til å lage rapport med html i stedet for xml.

De fleste avhengighetene hentes fra de vanlige sentrale repo-ene, med unntak av FxMapControl som er lagt ved som jar-fil i **libs**-mappa.
